#include <iostream>
#include <string>
using namespace std;

int main()
{
	string name("Daniel");
	cout << name << "\n" << "Word length: " << name.length() << "\n"
		<< "First char: " << name[0] << "\n" << "Last char: " << name[5]; 

	return 0;
}